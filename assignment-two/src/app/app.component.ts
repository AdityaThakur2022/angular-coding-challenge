import { Component } from '@angular/core';
import { StubService } from './stub.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  translator: Boolean;
  constructor(private service: StubService) {}

  swapState() {
    this.translator = !this.translator;
    this.service.changeTranslate(this.translator);
  }

}
