import { Component, OnInit } from '@angular/core';
import { StubService } from '../stub.service';

import ProfileInterface from '../interfaces/profile';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userData: ProfileInterface;
  constructor(public http: StubService) { }

  // Force pipe translation
  // TODO: Implement caching at service level for better throughput
  ngOnInit(): void {
    this.http.fetchProfile()
      .subscribe(res => {
        this.userData = res;
      })
  }
}
