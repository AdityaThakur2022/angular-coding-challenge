import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'app'
})
export class AppPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {

    // console.log(value, args);
    if (args[0] === true)
      return "T-" + value;

    return value;
  }
}
