import { Component, OnInit } from '@angular/core';
import { StubService } from '../stub.service';

import Posts from '../interfaces/posts';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  timelineData: Array<Posts>;
  constructor(private http: StubService) { }

  ngOnInit(): void {
    this.http.fetchPosts().subscribe(res => {
      this.timelineData = res;
    });
  }
}