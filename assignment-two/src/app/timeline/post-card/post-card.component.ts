import { Component, OnInit, Input } from '@angular/core';
import Posts from '../../interfaces/posts';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {

  constructor() {}
  @Input('data') data: Posts;

  ngOnInit(): void {
    // console.log(this.data);
  }

}
