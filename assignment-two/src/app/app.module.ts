import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { TimelineComponent } from './timeline/timeline.component';
import { EventsComponent } from './events/events.component';
import { HttpClientModule } from '@angular/common/http';

// Angular material dependencies
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { PostCardComponent } from './timeline/post-card/post-card.component';
import { BirthdayCardComponent } from './events/birthday-card/birthday-card.component';
import { AppPipe } from './app.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TimelineComponent,
    EventsComponent,
    PostCardComponent,
    BirthdayCardComponent,
    AppPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatSlideToggleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
