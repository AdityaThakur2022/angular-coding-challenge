import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { EventsComponent } from './events/events.component';
import { HomeComponent } from './home/home.component';
import { TimelineComponent } from './timeline/timeline.component';

// User-defined routes
const routes: Routes = [{
  path: '',
  component: HomeComponent,
},
{
  path: 'timeline',
  component: TimelineComponent
},
{
  path: 'events',
  component: EventsComponent
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
