import { Component, OnInit } from '@angular/core';
import { StubService } from '../stub.service';

import Friend from '../interfaces/events';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  friends: Array <Friend>;
  constructor(private http: StubService) { }

  ngOnInit(): void {
    this.http.fetchEvents().subscribe(res => {
        this.friends = res;
    });
  }
}
