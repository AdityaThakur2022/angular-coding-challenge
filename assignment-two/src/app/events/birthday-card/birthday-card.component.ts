import { Component, Input, OnInit } from '@angular/core';
import Birthday from '../../interfaces/events';

@Component({
  selector: 'app-birthday-card',
  templateUrl: './birthday-card.component.html',
  styleUrls: ['./birthday-card.component.css']
})
export class BirthdayCardComponent implements OnInit {

  constructor() { }
  ngOnInit(): void { }
  @Input('data') data: Birthday;

}
