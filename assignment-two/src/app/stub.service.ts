// Default imports
import { HttpClient } from '@angular/common/http';
import { InjectableCompiler } from '@angular/compiler/src/injectable_compiler';
import { Injectable } from '@angular/core';

// RxJS imports
import { Observable, from, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

// Local stub
import Data from './local-stub/events';

// Schema
import User from './interfaces/profile';

@Injectable({
  providedIn: 'root'
})

export class StubService {

  constructor(private http: HttpClient) { }

  // Memoization for reducing API calls
  translateMap: Map<string, string> = new Map();

  /* User data members */
  baseUrl: string = 'https://jsonplaceholder.typicode.com';
  translator: Boolean = false;

  changeTranslate(flag) {
    this.translator = flag;
    // console.log(this.translator);
  }

  /* PATH: '/posts' */
  fetchProfile(): any {
    return this.http.get(`${this.baseUrl}/users/1`, {
      observe: 'body',
      withCredentials: false,
      responseType: 'json'
    });
  }

  /* PATH: '/events' */
  fetchEvents(): Observable<any> {
    return of(Data);
  }

  /* PATH: '.' */
  fetchPosts(): Observable<any> {
    return this.http.get(`${this.baseUrl}/posts`, {
      observe: 'body',
      withCredentials: false,
      responseType: 'json'
    });
  }

  /**
   * @deprecated
   * @param User
   * @returns User
   */
  // TODO: Replace with actual translator API
  switchLanguage(val): User {

    console.log('Translation triggered')

    for (let key in val) {
      // console.log(key, typeof val[key]);
      if (typeof val[key] === 'string') {

        if (this.translateMap.get(key) === undefined) {
          console.log('Cached: ', key);
          let translatedData = 'T-' + val[key];
          this.translateMap.set(key, translatedData);
        }

        val[key] = this.translateMap.get(key);

      }
    }

    return val;
  }

}
