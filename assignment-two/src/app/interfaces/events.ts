export default interface EventInterface {
    "name": string,
    "age": number,
    "birthday": string
}