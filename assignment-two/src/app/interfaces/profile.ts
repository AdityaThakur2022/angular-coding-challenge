// Corresponds to JSON placeholder '/user/1' path `URI` 
export default interface ProfileInterface {
    "id": number;
    "name": string;
    "email": string;
    "address": {
        "street": string,
        "city": string,
        "zipcode": string,
        "geo": {
            "lat": number,
            "lng": number
        }
    };
    "phone": number;
    "website": string;
    "company": { "name": string }
}