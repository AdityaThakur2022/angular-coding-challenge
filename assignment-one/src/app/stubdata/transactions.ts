const transactions = [
    {
        uid: 1811981023,
        date: "Jun-28-2021",
        name: 'ABCD',
        amount: 1234,
        description: 'Match fixing'
    },
    {
        uid: 1811981024,
        date: "Jun-27-2021",
        name: 'CDEF',
        amount: 2341,
        description: 'Laundary'
    },
    {
        uid: 1811981025,
        date: "Jun-26-2021",
        name: 'GHIJ',
        amount: 3412,
        description: 'Press'
    },
    {
        uid: 1811981026,
        date: "Jun-25-2021",
        name: 'OPQR',
        amount: 4321,
        description: 'Truth'
    },
    {
        uid: 1811981027,
        date: "Jun-24-2021",
        name: 'STUV',
        amount: 3214,
        description: 'Unveil'
    }
]

export default transactions;