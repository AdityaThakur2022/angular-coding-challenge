// Mock data for 'Customers' module
const Customers = [
    {
        'uid': 1811981023,
        'name': 'ABCD',
        'phone': '+91-1234567890',
        'about': 'Hola Amigos!',
        'address': 'India'
    },
    {
        'uid': 1811981024,
        'name': 'CDEF',
        'phone': '+91-2345678901',
        'about': 'Hello!',
        'address': 'America'
    },
    {
        'uid': 1811981025,
        'name': 'EFGH',
        'phone': '+91-3456789012',
        'about': 'Hi!',
        'address': 'UK'
    },
    {
        'uid': 1811981026,
        'name': 'GHIJ',
        'phone': '+91-4567890123',
        'about': 'Good Morning..',
        'address': 'Australia'
    },
    {
        'uid': 1811981027,
        'name': 'GHIJ',
        'phone': '+91-4567890123',
        'about': 'Sayonara',
        'address': 'Japan'
    },
    {
        'uid': 1811981033,
        'name': 'ABCD',
        'phone': '+91-1234567890',
        'about': 'Hola Amigos!',
        'address': 'India'
    },
    {
        'uid': 1811981034,
        'name': 'CDEF',
        'phone': '+91-2345678901',
        'about': 'Hello!',
        'address': 'America'
    },
    {
        'uid': 1811981035,
        'name': 'EFGH',
        'phone': '+91-3456789012',
        'about': 'Hi!',
        'address': 'UK'
    },
    {
        'uid': 1811981036,
        'name': 'GHIJ',
        'phone': '+91-4567890123',
        'about': 'Good Morning..',
        'address': 'Australia'
    },
    {
        'uid': 1811981037,
        'name': 'GHIJ',
        'phone': '+91-4567890123',
        'about': 'Sayonara',
        'address': 'Japan'
    },

];

export default Customers;