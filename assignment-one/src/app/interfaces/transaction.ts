export default interface Transaction {
    uid: number;
    name: string;
    date: string;
    amount: number;
    description?: string;
}
