export default interface Customer {
    uid: number;
    name: string;
    phone: string;
    about?: string;
    address: string;
}