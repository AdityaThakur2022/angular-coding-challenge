import { Injectable } from '@angular/core';

// Stub
import CustomerData from './stubdata/customers';
import TransactionData from './stubdata/transactions';

// Interfaces
import Customer from './interfaces/customer';
import Transaction from './interfaces/transaction';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  customerData: Array<Customer>;
  transactionData: Array<Transaction>;

  constructor(private route: Router) {
    this.customerData = CustomerData;
    this.transactionData = TransactionData;
  }

  addTransaction(transaction): void {
    this.transactionData.push(transaction);
    this.route.navigate(['transaction']);
  }

  listCustomers(): Observable<any> {
    return of(this.customerData);
  }

  listTransactions(): Observable<any> {
    return of(this.transactionData);
  }

}