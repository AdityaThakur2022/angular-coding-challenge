import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

// RxJS imports
import { Observable, of, Subject, Subscription } from 'rxjs';
import { map, exhaustMap, exhaust, throttleTime } from 'rxjs/operators';

// Interfaces
import Customer from '../interfaces/customer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  /* Data members */
  customers: Array<Customer>;
  columns: Array<string> = ['id', 'name', 'about'];

  throttleSubject: Subject<any> = new Subject<any>();
  throttleHandler: Subscription;

  constructor(private data: AppService) { }

  ngOnInit(): void {

    this.data.listCustomers().subscribe(res => {
      this.customers = res;
    });

    this.throttleHandler = this.throttleSubject

      .pipe(
        exhaustMap(obs => this.wranglerUtility(obs)),
        throttleTime(800)
      )
      .subscribe(res => {
        console.log(res);
        this.data.addTransaction(res);
      });
  }

  /* Transaction view selector */
  selected: Object | null = null;

  viewCustomer(details) {
    this.selected = details;
  }

  addNewTransaction(details): void {
    this.throttleSubject.next(details);
  }

  wranglerUtility(obj): Observable<any> {

    return of(obj)
      .pipe(
        map(data => {
          return {
            uid: data?.uid,
            name: data?.name,
            date: Date().slice(4, 15).replace(' ', '-').replace(' ', '-'),
            amount: 1000,
            description: data?.about
          }
        })
      )
  }
}
