import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

import Transaction from '../interfaces/transaction';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  transactions: Array<Transaction>;
  columns: Array<string> = ['date', 'name', 'amount', 'description'];

  constructor(private data: AppService) { }

  ngOnInit(): void {
    this.data.listTransactions().subscribe(res => {
      this.transactions = res;
    });
  }

  /* Transaction view selector */
  selected: Object | null = null;
  viewTransaction(details) {
    this.selected = details
  }
}
